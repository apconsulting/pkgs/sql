package sql

import (
	"database/sql"
	"encoding/json"
)

type NullFloat64 struct {
	sql.NullFloat64
}

func (nf NullFloat64) MarshalJSON() (b []byte, err error) {
	return json.Marshal(nf.Float64)
}

func (nf *NullFloat64) UnmarshalJSON(b []byte) (err error) {
	var nfIn any
	err = json.Unmarshal(b, &nfIn)
	if err != nil {
		return err
	}
	return nf.Scan(nfIn)
}
